﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AJS.JiraSoapService.Contracts;
using Atlassian.Jira.Remote;
using Moq;
using NUnit.Framework;

namespace AJS.JiraSoapService.Tests
{
    public class JiraSoapServiceTests
    {
        private IJiraSoapClient _serviceClient;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            var settings = new Mock<IServiceSettings>();
            settings.SetupGet(x => x.BindingConfigurationName).Returns("JiraSoapHttps");
            settings.SetupGet(x => x.EndpointAddress).Returns("https://jira.url/rpc/soap/jirasoapservice-v2");
            settings.SetupGet(x => x.ServiceUsername).Returns("user");
            settings.SetupGet(x => x.ServicePassword).Returns("pass");

            _serviceClient = new JiraSoapService(settings.Object);
        }

        [Test]
        public void ServiceClient_CanCreate()
        {
            Assert.NotNull(_serviceClient);
        }

        [Test]
        public void ServiceClient_CanGetUser()
        {
            var usr = _serviceClient.GetUser("username");
            Assert.NotNull(usr);
        }

        [Test]
        public void ServiceClient_CanCreateProject()
        {
            var prj = _serviceClient.CreateProject(
                "TestProj",
                "TPJ",
                "This is an automated project from Back Office",
                null,
                "aaron.sempf");
            Assert.NotNull(prj);
        }

        [Test]
        public void ServiceClient_CanCreateIssue()
        {
            var projectKey = "TPJ";

            var typs = _serviceClient.GetProjectIssueTypes(projectKey);
            var typId = typs.Where(x => x.name == "Task").Select(x => x.id).FirstOrDefault();
            var prios = _serviceClient.GetPriorities();
            var prioId = prios.Where(x => x.name == "Normal").Select(x => x.id).FirstOrDefault();
            var flds = _serviceClient.GetRemoteFields();
            var fld = flds.FirstOrDefault(x => x.name == "Epic Name");
            var fldVal = new RemoteCustomFieldValue
            {
                customfieldId = fld.id,
                values = new[] { "Functional" }
            };
            var issue = _serviceClient.CreateIssue(
                "TPJ",
                typId,
                prioId,
                "This is a Task with Epic",
                "This is an automated Task from BackOffice",
                null,
                new[] { fldVal });

            Assert.NotNull(issue);
        }

        [Test]
        public void ServiceClient_CanGetRemoteFields()
        {
            var flds = _serviceClient.GetRemoteFields();

            Assert.NotNull(flds);
        }

        [Test]
        public void ServiceClient_CanCreateRemoteFieldValue()
        {
            var flds = _serviceClient.GetRemoteFields();
            var fld = flds.FirstOrDefault(x => x.name == "Epic Name");
            var fldVal = new RemoteCustomFieldValue
            {
                customfieldId = fld.id,
                values = new[] { "Functional" }
            };

            Assert.NotNull(fldVal);
        }
    }
}
