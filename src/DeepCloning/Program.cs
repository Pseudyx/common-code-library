﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using Omu.ValueInjecter;

namespace DeepCloning
{
    class Program
    {
        static void Main()
        {
            const int Iterations = 10000;
            var watch = new Stopwatch();

            var foo = GetFoo();

            CheckDeepCloning();

            Console.WriteLine("start test, iterations = " + Iterations);

            watch.Restart();
            for (int i = 0; i < Iterations; i++)
            {
                new Foo().InjectFrom<DeepCloneInjection>(foo);
            }
            watch.Stop();
            Console.WriteLine("DeepCloning: " + watch.Elapsed);
            watch.Reset();

            watch.Restart();
            for (int i = 0; i < Iterations; i++)
            {
                new Foo().InjectFrom<FastDeepCloneInjection>(foo);
            }
            watch.Stop();
            Console.WriteLine("Faster DeepCloning with FastMember: " + watch.Elapsed);
            watch.Reset();


            watch.Restart();
            for (int i = 0; i < Iterations; i++)
            {
                new Foo().InjectFrom<OldDeepCloneInjection>(foo);
            }
            watch.Stop();
            Console.WriteLine("Old deep clone injection: " + watch.Elapsed);
            watch.Reset();
        }

        private static Foo GetFoo()
        {
            var o = new Foo
            {
                Name = "foo",
                Int32 = 12,
                Int64 = 123123,
                NullInt = 16,
                DateTime = DateTime.Now,
                Doublen = 2312112,
                Foo1 = new Foo { Name = "foo one" },
                Foos = new List<Foo>
                                       {
                                           new Foo {Name = "j1", Int64 = 123, NullInt = 321},
                                           new Foo {Name = "j2", Int32 = 12345, NullInt = 54321},
                                           new Foo {Name = "j3", Int32 = 12345, NullInt = 54321},
                                       },
                FooArr = new[]
                                         {
                                             new Foo {Name = "a1"},
                                             new Foo {Name = "a2"},
                                             new Foo {Name = "a3"},
                                         },
                IntArr = new[] { 1, 2, 3, 4, 5 },
                Ints = new[] { 7, 8, 9 },
            };

            return o;
        }

        private static void CheckDeepCloning()
        {
            var o = GetFoo();

            var c = new Foo().InjectFrom<DeepCloneInjection>(o) as Foo;

            Assert.AreEqual(o.Name, c.Name);
            Assert.AreEqual(o.Int32, c.Int32);
            Assert.AreEqual(o.Int64, c.Int64);
            Assert.AreEqual(o.NullInt, c.NullInt);
            Assert.AreEqual(o.IntArr, c.IntArr);
            Assert.AreEqual(o.Ints, c.Ints);
            Assert.AreEqual(o.DateTime, o.DateTime);

            Assert.AreNotEqual(o.Foo1, c.Foo1);
            Assert.AreNotEqual(o.Foos, c.Foos);
            Assert.AreNotEqual(o.FooArr, c.FooArr);

            //Foo F1
            Assert.AreEqual(o.Foo1.Name, c.Foo1.Name);

            //Foo[] FooArr
            Assert.AreEqual(o.FooArr.Length, c.FooArr.Length);
            Assert.AreNotEqual(o.FooArr[0], c.FooArr[0]);
            Assert.AreEqual(o.FooArr[0].Name, c.FooArr[0].Name);

            //IEnumerable<Foo> Foos
            Assert.AreEqual(o.Foos.Count(), c.Foos.Count());
            Assert.AreNotEqual(o.Foos.First(), c.Foos.First());
            Assert.AreEqual(o.Foos.First().Name, c.Foos.First().Name);
        }
    }

    public class Foo
    {
        public string Name { get; set; }

        public int Int32 { get; set; }

        public long Int64 { set; get; }

        public int? NullInt { get; set; }

        public float Floatn { get; set; }

        public double Doublen { get; set; }

        public DateTime DateTime { get; set; }

        public Foo Foo1 { get; set; }

        public IEnumerable<Foo> Foos { get; set; }

        public Foo[] FooArr { get; set; }

        public int[] IntArr { get; set; }

        public IEnumerable<int> Ints { get; set; }
    }
}
