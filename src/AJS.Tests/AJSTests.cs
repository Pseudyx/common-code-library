﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using NCalc;
using NUnit.Framework;

namespace AJS.Tests
{
    [TestFixture]
    public class AJSTests
    {
        [Test]
        public void CanGetWithNullSetting()
        {
            string[] algebra = {"C", "A"};
            double[] values = {1, 3};

            List<KeyValuePair<string,double>> dictionary = new List<KeyValuePair<string, double>>();

            for (int i = 0; i < algebra.Count(); i++)
            {
                dictionary.Add(new KeyValuePair<string, double>(algebra[i],values[i]));
            }

            var formula = "(C*5)+2.5-(A*7)/2";
            var partCount = Regex.Matches(formula, @"[(]", RegexOptions.Singleline | RegexOptions.Compiled).Count;

            List<string> parts = new List<string>();
            List<KeyValuePair<string, double>> partvalue = new List<KeyValuePair<string, double>>();

            var newFormula = formula;
            if (partCount != 0)
            {
                for (int i = 0; i < partCount; i++)
                {
                    var sI = newFormula.IndexOf('(');
                    var eI = newFormula.IndexOf(')')+1;
                    var charCount = eI - sI;
                    var sPart = newFormula.Substring(sI, charCount);

                    parts.Add(sPart);
                    newFormula = newFormula.Replace(sPart, "");
                }
            }

            var newParts = new List<KeyValuePair<string,string>>();
            parts.ForEach(part =>
            {
                MatchCollection matches = Regex.Matches(part, @"[A-Za-z]", RegexOptions.Singleline | RegexOptions.Compiled);
                if (matches.Count > 0)
                {
                    for (int i = 0; i < matches.Count; i++)
                    {
                        Match match = matches[i];
                        var val = dictionary.FirstOrDefault(x => x.Key == match.Value);
                        newParts.Add(new KeyValuePair<string, string>(part,
                            part.Replace(match.Value, val.Value.ToString())));
                    }
                }
                else
                {
                    newParts.Add(new KeyValuePair<string, string>(part,part));
                }
            });

            newParts.ForEach(part =>
            {
                formula = formula.Replace(part.Key, part.Value);
            });

            Expression e = new Expression(formula);
            double returnVal; double.TryParse(e.Evaluate().ToString(), out returnVal);
           
            Assert.IsNotNull(returnVal);
        }
    }
}
