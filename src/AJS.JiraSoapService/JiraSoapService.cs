﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using AJS.JiraSoapService.Contracts;
using Atlassian.Jira.Remote;

namespace AJS.JiraSoapService
{
    public class JiraSoapService : IJiraSoapClient
    {
        private readonly JiraSoapServiceClient _jiraSoapServiceClient;

        public JiraSoapService(IServiceSettings serviceSettings)
        {
            _jiraSoapServiceClient = new JiraSoapServiceClient(serviceSettings.BindingConfigurationName);
            _jiraSoapServiceClient.Endpoint.Address = new EndpointAddress(serviceSettings.EndpointAddress);
            AuthToken = _jiraSoapServiceClient.login(serviceSettings.ServiceUsername, serviceSettings.ServicePassword);
        }
        public static JiraSoapService GetClient()
        {
            return new JiraSoapService(Configuration.SoapServiceConfiguration.Settings);
        }

        public string AuthToken { get; private set; }

        public RemoteProject GetProject(string projectKey)
        {
            try
            {
                return _jiraSoapServiceClient.getProjectByKey(AuthToken, projectKey);
            }
            catch { return null; }
        }
        public RemoteUser GetUser(string username)
        {
            return _jiraSoapServiceClient.getUser(AuthToken, username);
        }
        
        public List<RemotePriority> GetPriorities()
        {
            return _jiraSoapServiceClient.getPriorities(AuthToken).ToList();
        }
        public List<RemoteIssueType> GetProjectIssueTypes(string projectKey)
        {
            var prj = GetProject(projectKey);

            return GetProjectIssueTypes(prj);
        }
        public List<RemoteIssueType> GetProjectIssueTypes(RemoteProject project)
        {
            return _jiraSoapServiceClient.getIssueTypesForProject(AuthToken, project.id).ToList();
        }
        public List<RemoteField> GetRemoteFields()
        {
            return _jiraSoapServiceClient.getCustomFields(AuthToken).ToList();
        }
        
        public RemoteProject CreateProject(string projectName, string key, string description, string leadUsername, string url=null)
        {
            var proj = GetProject(key);
            if (proj != null) return proj;

            try
            {

                var permission = new RemotePermissionScheme();
                var scheme = new RemoteScheme();

                return _jiraSoapServiceClient.createProject(AuthToken, key, projectName, description, url, leadUsername,
                    permission, scheme, scheme);
            }
            catch { return null; }
        }
        public void CreateProject(string projectName, string key, string description, string leadUsername, string url = null, bool supress = true)
        {
            var proj = CreateProject(projectName, key, description, leadUsername, url);   
        }

        public RemoteIssue AddProjectIssue(RemoteIssue issue)
        {
            return _jiraSoapServiceClient.createIssue(AuthToken, issue);
        }
        public RemoteIssue CreateIssue(string projectKey, string issueTypeId, string priorityId, string summary,
            string description, string assignee = null, RemoteCustomFieldValue[] customFieldValues = null, bool saveToJira = true)
        {
            var issue = new RemoteIssue
            {
                project = projectKey,
                type = issueTypeId,
                priority = priorityId,
                summary = summary,
                description = description,
                assignee = assignee,
                customFieldValues = customFieldValues
            };

            return saveToJira ? AddProjectIssue(issue) : issue;
        }


    }
}
