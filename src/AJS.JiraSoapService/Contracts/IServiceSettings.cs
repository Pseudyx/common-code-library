﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AJS.JiraSoapService.Contracts
{
    public interface IServiceSettings
    {
        string EndpointAddress { get; }
        string BindingConfigurationName { get; }
        string ServiceUsername { get; }
        string ServicePassword { get; }
    }
}
