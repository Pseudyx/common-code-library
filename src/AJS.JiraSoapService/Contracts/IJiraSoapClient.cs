﻿using System.Collections.Generic;
using Atlassian.Jira.Remote;

namespace AJS.JiraSoapService.Contracts
{
    public interface IJiraSoapClient
    {
        string AuthToken { get; }

        RemoteProject GetProject(string projectKey);
        RemoteUser GetUser(string username);

        List<RemotePriority> GetPriorities();
        List<RemoteIssueType> GetProjectIssueTypes(string projectKey);
        List<RemoteIssueType> GetProjectIssueTypes(RemoteProject project);
        List<RemoteField> GetRemoteFields();

        RemoteProject CreateProject(string projectName, string key, string description, string url, string leadUsername);
        void CreateProject(string projectName, string key, string description, string leadUsername, string url = null, bool supress = true);
        
        RemoteIssue AddProjectIssue(RemoteIssue issue);
        RemoteIssue CreateIssue(string projectKey, string issueTypeId, string priorityId, string summary,
            string description, string assignee = null, RemoteCustomFieldValue[] customFieldValues = null, bool saveToJira = true);

    }
}
