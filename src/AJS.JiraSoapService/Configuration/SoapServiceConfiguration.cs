﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AJS.JiraSoapService.Contracts;

namespace AJS.JiraSoapService.Configuration
{
    class SoapServiceConfiguration : ConfigurationSection, IServiceSettings
    {
        private static readonly SoapServiceConfiguration settings = ConfigurationManager.GetSection("jiraSoapServiceConfiguration") as SoapServiceConfiguration;

        public static IServiceSettings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("endpointAddress", IsRequired = true)]
        public string EndpointAddress
        {
            get { return (string)this["endpointAddress"]; }
        }

        [ConfigurationProperty("bindingConfigurationName", IsRequired = true)]
        public string BindingConfigurationName
        {
            get { return (string)this["bindingConfigurationName"]; }
        }

        [ConfigurationProperty("serviceUsername", IsRequired = true)]
        public string ServiceUsername
        {
            get { return (string)this["serviceUsername"]; }
        }

        [ConfigurationProperty("servicePassword", IsRequired = true)]
        public string ServicePassword
        {
            get { return (string)this["servicePassword"]; }
        }
    }
}
