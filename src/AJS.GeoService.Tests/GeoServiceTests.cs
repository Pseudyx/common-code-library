﻿using System.Web;
using AJS.GeoService.Configuration;
using AJS.GeoService.Contracts;
using AJS.GeoService.Service;
using NUnit.Framework;

namespace AJS.GeoService.Tests
{
    [TestFixture]
    public class GeoServiceTests
    {
        private const string Ip = "59.167.38.9";

        private static T Get<T>(ApiMethods method, IApiQuery query)
        {
            using (var client = new GeoIPClient(new SettingsStub()))
            {
                var rObj = client.Get<T>(method, query);
                return rObj;
            }

            return default(T);
        }

        private static T NullGet<T>(ApiMethods method, ApiQuery query)
        {
            using (var client = new GeoIPClient())
            {
                var rObj = client.Get<T>(method, query);
                return rObj;
            }

            return default(T);
        }

        [Test]
        public void CanGetWithNullSetting()
        {
            var apiQuery = new ApiQuery { Ip = Ip };
            var result = NullGet<GeoIP>(ApiMethods.json, apiQuery);
            Assert.NotNull(result);
        }

        [Test]
        public void CanGetWithSetting()
        {
            var apiQuery = new ApiQuery { Ip = Ip };
            var result = Get<GeoIP>(ApiMethods.json, apiQuery);
            Assert.NotNull(result);
        }

        [Test]
        public void CanGetMethod_GetGeoIP()
        {
            var result = GeoService.GetGeoIP(Ip);
            Assert.NotNull(result);
        }

        [Test]
        public void CanGetMethod_GetGeoIPAsJson()
        {
            var result = GeoService.GetGeoIPasJson(Ip);
            Assert.NotNull(result);
        }

        [Test]
        public void CanGetMethod_GetCountryCode()
        {
            var result = GeoService.GetCountryCode(Ip);
            Assert.NotNull(result);
        }

        [Test]
        public void CanGetMethod_GetCountryCodeCookie()
        {
            HttpCookie result = GeoService.GetCountryCodeCookie(Ip);
            Assert.NotNull(result);
        }

    }

    public class SettingsStub : IServiceSettings
    {
        public string Url
        {
            get { return "http://freegeoip.net/"; }
        }
        public string Username
        {
            get { return null; }
        }
        public string Password
        {
            get { return null; }
        }
        public string Ip
        {
            get { return null; }
        }
    }
}
