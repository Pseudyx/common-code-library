﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using Omu.ValueInjecter;

namespace AJS
{
    public static class Utility
    {
        public static bool Equals(object objSource, object objTarget, string[] skipProperties = null)
        {
            var isEqual = (objSource != null && objTarget != null);
            if (!isEqual) return false;

            objSource.GetType().GetProperties().ToList().ForEach(sp =>
            {
                if (!isEqual) return;
                objTarget.GetType().GetProperties().ToList().ForEach(tp =>
                {
                    if (!isEqual) return;
                    if (sp.Name == tp.Name)
                    {
                        if (skipProperties != null && skipProperties.Contains(sp.Name)) return;

                        object SourceVal = sp.GetValue(objSource);
                        object TargetVal = tp.GetValue(objTarget);

                        if (SourceVal == null) return;

                        //Compare simple value types
                        if (sp.PropertyType.IsValueType || sp.PropertyType == typeof (string))
                        {
                            if (string.IsNullOrEmpty(SourceVal.ToString())) return;
                            if (TargetVal == null)
                            {
                                isEqual = false;
                                return;
                            }
                            isEqual = (!string.IsNullOrEmpty(TargetVal.ToString()));
                            if (!isEqual) return;

                            var sourceDecode = HttpUtility.HtmlDecode(SourceVal.ToString());
                            var targetDecode = HttpUtility.HtmlDecode(TargetVal.ToString());

                            isEqual = sourceDecode.Equals(targetDecode);
                            return;
                        }

                        if (sp.PropertyType.IsArray)
                        {
                            var arrS = SourceVal as Array;
                            var arrT = TargetVal as Array;

                            int indexS = arrS.Length;
                            int indexT = arrT.Length;

                            isEqual = (indexS == indexT);
                            if (!isEqual) return;

                            for (int ai = 0; ai < indexS; ai++)
                            {
                                if (!isEqual) return;
                                object propSource = arrS.GetValue(ai);
                                object propTarget = arrT.GetValue(ai);

                                isEqual = Equals(propSource, propTarget, skipProperties);
                            }
                        }

                        if (sp.PropertyType.IsClass &&
                            !(sp.PropertyType.IsEnum || sp.PropertyType.FullName.Contains("System.Collections")))
                        {
                            if (SourceVal == null) return;
                            isEqual = (TargetVal != null);
                            if (!isEqual) return;

                            isEqual = Equals(SourceVal, TargetVal, skipProperties);
                            return;
                        }

                        //handle IEnumerable<>, ICollection<>, IList<>, List<>
                        if (sp.PropertyType.GetGenericTypeDefinition().GetInterfaces().Contains(typeof (IEnumerable)))
                        {
                            #region Source List

                            //Initialise Source List
                            Type genericSArgument = sp.PropertyType.GetGenericArguments()[0];
                            Type sourceTlist = typeof (List<>).MakeGenericType(genericSArgument);
                            object sourcelist = Activator.CreateInstance(sourceTlist);

                            //Add all source generic and simple types to list
                            if (genericSArgument.IsValueType || genericSArgument == typeof (string))
                            {
                                MethodInfo addRange = sourceTlist.GetMethod("AddRange");
                                addRange.Invoke(sourcelist, new[] {SourceVal});
                            }
                            else
                            {
                                MethodInfo addMethod = sourceTlist.GetMethod("Add");
                                foreach (object sObj in SourceVal as IEnumerable)
                                {
                                    addMethod.Invoke(sourcelist,
                                        new[] {Activator.CreateInstance(genericSArgument).InjectFrom(sObj)});
                                }
                            }

                            //convert filled source List to Array
                            var getSourceCount = sourceTlist.GetProperty("Count");
                            var sourceCount = 0;
                            var countValS = getSourceCount.GetValue(sourcelist).ToString();
                            int.TryParse(countValS, out sourceCount);

                            #endregion

                            if (sourceCount == 0 && TargetVal == null) return;

                            #region Target List

                            //Initialise Target list
                            var genericTArgument = tp.PropertyType.GetGenericArguments()[0];
                            var targetTlist = typeof (List<>).MakeGenericType(genericTArgument);
                            var targetlist = Activator.CreateInstance(targetTlist);

                            //Add all source generic and simple types to list
                            if (genericTArgument.IsValueType || genericTArgument == typeof (string))
                            {
                                MethodInfo addRange = targetTlist.GetMethod("AddRange");
                                addRange.Invoke(targetlist, new[] {TargetVal});
                            }
                            else
                            {
                                MethodInfo addMethod = targetTlist.GetMethod("Add");
                                foreach (object tObj in TargetVal as IEnumerable)
                                {
                                    addMethod.Invoke(targetlist,
                                        new[] {Activator.CreateInstance(genericTArgument).InjectFrom(tObj)});
                                }
                            }

                            //convert filled target List to Array
                            var getTargetCount = targetTlist.GetProperty("Count");
                            var targetCount = 0;
                            var countValT = getTargetCount.GetValue(targetlist).ToString();
                            int.TryParse(countValT, out targetCount);

                            #endregion

                            isEqual = (sourceCount == targetCount);
                            if (!isEqual) return;

                            #region Convert Source and Target to Array

                            Array newSourceArr = Array.CreateInstance(genericSArgument, sourceCount);
                            int sIndex = 0;
                            foreach (object sObj in sourcelist as IEnumerable)
                            {
                                newSourceArr.SetValue(sObj, sIndex);
                                sIndex++;
                            }

                            Array newTargetArr = Array.CreateInstance(genericTArgument, targetCount);
                            int tIndex = 0;
                            foreach (object tObj in targetlist as IEnumerable)
                            {
                                newTargetArr.SetValue(tObj, tIndex);
                                tIndex++;
                            }

                            #endregion

                            //Compare Source and Target Array
                            for (int si = 0; si < sourceCount; si++)
                            {
                                if (!isEqual) return;
                                object propSource = newSourceArr.GetValue(si);
                                object propTarget = newTargetArr.GetValue(si);

                                isEqual = Equals(propSource, propTarget, skipProperties);
                            }
                        }
                    }
                });
            });

            return isEqual;
        }
    }

    public static class Events
    {
        public static Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            string assmbPath = "";

            var objExecutingAssemblies = Assembly.GetExecutingAssembly();
            var codeBase = objExecutingAssemblies.CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var assemblyPath = Path.GetDirectoryName(path);
            var arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

            if (
                arrReferencedAssmbNames.Any(
                    strAssmbName =>
                        strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",", StringComparison.Ordinal)) ==
                        args.Name.Substring(0, args.Name.IndexOf(",", StringComparison.Ordinal))))
            {
                assmbPath = assemblyPath + "\\" +
                            args.Name.Substring(0, args.Name.IndexOf(",", StringComparison.Ordinal)) + ".dll";
            }

            var foundAssembly = Assembly.LoadFrom(assmbPath);
            return foundAssembly;
        }
    }
}