﻿using System;
using System.Device.Location;
using System.Web;
using AJS.GeoService.Configuration;
using AJS.GeoService.Service;
using Newtonsoft.Json;

namespace AJS.GeoService
{
    public static class GeoService
    {
        public static GeoIPClient GetClient()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(AssemblyResolver.ResolveEventHandler);

            return new GeoIPClient(ServiceConfigurationSection.Settings);
        }

        private static T Get<T>(ApiMethods method, ApiQuery query)
        {
            using (var client = GetClient())
            {
                var rObj = client.Get<T>(method, query);
                return rObj;
            }

            return default(T);
        }

        public static GeoIP GetGeoIP(string IpAddress)
        {
            var apiQuery = new ApiQuery { Ip = IpAddress };
            return Get<GeoIP>(ApiMethods.json, apiQuery);
        }

        public static string GetGeoIPasJson(string IpAddress)
        {
            var apiQuery = new ApiQuery { Ip = IpAddress };
            var IpData = Get<GeoIP>(ApiMethods.json, apiQuery);
            
            var settings = new JsonSerializerSettings() { ContractResolver = new Serializer.JsonContractResolver() };
            return JsonConvert.SerializeObject(IpData, Formatting.None, settings);
        }

        public static string GetCountryCode(string IpAddress)
        {
            var geoData = GetGeoIP(IpAddress);
            return geoData.CountryCode;
        }

        public static HttpCookie GetCountryCodeCookie(string IpAddress)
        {
            var countryCode = GetCountryCode(IpAddress);
            var countryCodeCookie = new HttpCookie("countryCode", countryCode);
            return countryCodeCookie;
        }

        public static void SetCountryCodeCookie(HttpResponse ResponseContext, string IpAddress)
        {
            var countryCodeCookie = GetCountryCodeCookie(IpAddress);
            ResponseContext.Cookies.Add(countryCodeCookie);
        }

        public static HttpCookie GetGeoIPCookie(string IpAddress)
        {
            var geoIP = GetGeoIP(IpAddress);
            var GeoIPCookie = new HttpCookie("GeoIP");

            GeoIPCookie.Values.Add("ip", geoIP.Ip);
            GeoIPCookie.Values.Add("countryCode", geoIP.CountryCode);
            GeoIPCookie.Values.Add("countryName", geoIP.CountryName);
            GeoIPCookie.Values.Add("regionCode", geoIP.RegionCode);
            GeoIPCookie.Values.Add("regionName", geoIP.RegionName);
            GeoIPCookie.Values.Add("city", geoIP.City);
            GeoIPCookie.Values.Add("zipCode", geoIP.ZipCode);
            GeoIPCookie.Values.Add("latitude", geoIP.GeoCoordinate.Latitude.ToString());
            GeoIPCookie.Values.Add("longitude", geoIP.GeoCoordinate.Longitude.ToString());
            GeoIPCookie.Values.Add("metroCode", geoIP.MetroCode);
            GeoIPCookie.Values.Add("areaCode", geoIP.AreaCode);

            return GeoIPCookie;
        }

        public static void SetGeoIPCookie(HttpResponse ResponseContext, string IpAddress)
        {
            var GeoIPCookie = GetGeoIPCookie(IpAddress);
            ResponseContext.Cookies.Add(GeoIPCookie);
        }

    }

    public class GeoIP
    {
        [JsonProperty("ip")]
        public string Ip { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("country_name")]
        public string CountryName { get; set; }
        [JsonProperty("region_code")]
        public string RegionCode { get; set; }
        [JsonProperty("region_name")]
        public string RegionName { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("zipcode")]
        public string ZipCode { get; set; }
        [JsonProperty("latitude")]
        private double Latitude { get; set; }
        [JsonProperty("longitude")]
        private double Longitude { get; set; }
        [JsonProperty("metro_code")]
        public string MetroCode { get; set; }
        [JsonProperty("area_code")]
        public string AreaCode { get; set; }

        public GeoCoordinate GeoCoordinate
        {
            get
            {
                return new GeoCoordinate { Latitude = Latitude, Longitude = Longitude };
            }
        }
    }
}
