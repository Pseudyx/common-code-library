﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AJS.GeoService.Configuration;
using AJS.GeoService.Service;

namespace AJS.GeoService.Contracts
{
    public interface IGeoIPClient : IDisposable
    {
        Task<T> GetAsync<T>(ApiMethods apiMethod, IApiQuery query = null);
        T Get<T>(ApiMethods apiMethod, IApiQuery query = null);
        HttpClient Client { get; }
    }
}
