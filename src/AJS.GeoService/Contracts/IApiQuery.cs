namespace AJS.GeoService.Contracts
{
    public interface IApiQuery
    {
        string Ip { get; set; }
        string Value { get; }
    }
}