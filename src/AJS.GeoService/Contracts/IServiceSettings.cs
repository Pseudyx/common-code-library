﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AJS.GeoService.Contracts
{
    public interface IServiceSettings
    {
        string Url { get; }
        string Username { get; }
        string Password { get; }
        string Ip { get; }
    }
}
