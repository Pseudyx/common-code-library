﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AJS.GeoService.Configuration;
using AJS.GeoService.Contracts;

namespace AJS.GeoService.Service
{
    public class GeoIPClient : IGeoIPClient
    {
        private readonly IServiceSettings serviceSettings;

        public HttpClient Client { get; private set; }

        public GeoIPClient(IServiceSettings serviceSettings = null)
        {
            if (serviceSettings != null)
            {
                Client = new HttpClient {BaseAddress = new Uri(serviceSettings.Url)};
                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrEmpty(serviceSettings.Username))
                {
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", serviceSettings.Username,serviceSettings.Password))));
                }
            }
            else
            {
                Client = new HttpClient { BaseAddress = new Uri("http://freegeoip.net/") };
                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
        }

        public async Task<T> GetAsync<T>(ApiMethods apiMethod, IApiQuery query = null)
        {
            var response = (query != null) ? await Client.GetAsync(string.Format("{0}/{1}", apiMethod, query.Value)) : await Client.GetAsync(string.Format("{0}/", apiMethod));
            return await response.Content.ReadAsAsync<T>();
        }

        public T Get<T>(ApiMethods apiMethod, IApiQuery query = null)
        {
            var response = (query != null) ? Client.GetAsync(string.Format("{0}/{1}", apiMethod, query.Value)).Result : Client.GetAsync(string.Format("{0}/", apiMethod)).Result;
            return response.Content.ReadAsAsync<T>().Result;
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }

   
}
