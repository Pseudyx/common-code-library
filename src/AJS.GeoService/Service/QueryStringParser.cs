﻿using System.Collections.Specialized;
using System.Web;

namespace AJS.GeoService.Service
{
    public class QueryStringParser
    {
        private readonly NameValueCollection _parameters;

        public QueryStringParser()
        {
            _parameters = HttpUtility.ParseQueryString(string.Empty);
        }

        public override string ToString()
        {
            return _parameters.HasKeys() ? string.Format("?{0}", _parameters) : string.Empty;
        }

        public void AddParameter(string name, object value)
        {
            _parameters[name] = value.ToString();
        }
    }
}
