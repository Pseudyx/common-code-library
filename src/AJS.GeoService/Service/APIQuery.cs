﻿using AJS.GeoService.Contracts;

namespace AJS.GeoService.Service
{
    public class ApiQuery : IApiQuery
    {
        private readonly QueryStringParser queryStringParser;

        public string Ip { get; set; }
        
        public ApiQuery(string ip = null)
            : this()
        {
            Ip = ip ?? null;
        }

        public ApiQuery()
        {
            queryStringParser = new QueryStringParser();
        }

        public string Value
        {
            get
            {
                if (!string.IsNullOrEmpty(Ip))
                    queryStringParser.AddParameter("q", Ip);

                return queryStringParser.ToString().ToLower();
            }
        }
    }
}
