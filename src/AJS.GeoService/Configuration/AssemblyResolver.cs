﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AJS.GeoService.Configuration
{
    public static class AssemblyResolver
    {
        public static Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            var assmbPath = "";

            var objExecutingAssemblies = Assembly.GetExecutingAssembly();
            var codeBase = objExecutingAssemblies.CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            var assemblyPath = Path.GetDirectoryName(path);
            var arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

            if (arrReferencedAssmbNames.Any(strAssmbName => strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",", System.StringComparison.Ordinal)) == args.Name.Substring(0, args.Name.IndexOf(",", System.StringComparison.Ordinal))))
            {
                assmbPath = assemblyPath + "\\" + args.Name.Substring(0, args.Name.IndexOf(",", System.StringComparison.Ordinal)) + ".dll";
            }
            var foundAssembly = Assembly.LoadFrom(assmbPath);
            return foundAssembly;
        }
    }
}
