﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AJS.GeoService.Contracts;

namespace AJS.GeoService.Configuration
{
    public class ServiceConfigurationSection : ConfigurationSection, IServiceSettings
    {
        private static readonly IServiceSettings pSettings = ConfigurationManager.GetSection("serviceConfiguration") as IServiceSettings;
        public static IServiceSettings Settings
        {
            get { return pSettings; }
        }

        [ConfigurationProperty("Url", IsRequired = true)]
        public string Url { get { return (string)this["Url"]; } }

        [ConfigurationProperty("username", IsRequired = false)]
        public string Username { get { return (string)this["username"]; } }

        [ConfigurationProperty("password", IsRequired = false)]
        public string Password { get { return (string)this["password"]; } }

        [ConfigurationProperty("Ip", IsRequired = false)]
        public string Ip { get { return (string)this["Ip"]; } }
    }

    public enum ApiMethods
    {
        csv,xml,json
    }
}
